package rs.sga.gdi18;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.instrument.Instrumentation;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonWriter;

import rs.sga.gdi18.hibernate.DataAccessLayer;
import rs.sga.gdi18.hibernate.TransactionalCode;

@Path("/picture")
public class PictureServlet {
	private static int imageCounter = 0;
	private static final String BASE_IMAGE_URL = "http://192.168.0.13:8080/images/";
	
	@GET
	@Path("delete2")
	public String delete() {
		DataAccessLayer.transactional(new TransactionalCode<Void>() {

			@Override
			public Void run(Session session) {
				Query query = session.createQuery("delete from RelatedPicture");
				query.executeUpdate();
				
				query = session.createQuery("delete from Picture");
				query.executeUpdate();
				
				return null;
			}
		});
		
		return "success";
	}
	
	@POST
	@Path("save")
	public String savePicture(String jsonString) {
		
        DataAccessLayer.transactional(new TransactionalCode<Void>() {

			@Override
			public Void run(Session session) {
				try {
					Type type = new TypeToken<Picture>() {}.getType();
			        Picture picture = new Gson().fromJson(jsonString, type);
			        
			        for(RelatedPicture relatedPicture : picture.getRelatedPictures()) {
			        	//save related picture to a file
			        	byte[] byteArray = Base64.getDecoder().decode(relatedPicture.getPictureBlob());
			        	
			        	String fileName = "image" + (imageCounter++) + ".jpg";
			        	File file = new File("images/" + fileName);
						OutputStream outputStream = new FileOutputStream(file);
						outputStream.write(byteArray);
						outputStream.close();
			        	
						relatedPicture.setPictureBlob(BASE_IMAGE_URL + fileName);
			        }
			        
			        //save picture to a file
			        byte[] byteArray = Base64.getDecoder().decode(picture.getPictureBlob());
			        
			        String fileName = "image" + (imageCounter++) + ".jpg";
		        	File file = new File("images/" + fileName);
					OutputStream outputStream = new FileOutputStream(file);
					outputStream.write(byteArray);
					outputStream.close();
					
					picture.setPictureBlob(BASE_IMAGE_URL + fileName);
					
					session.save(picture);
				} catch (IOException e) { e.printStackTrace(); }
				return null;
			}
		});
        
        return "Success";
	}

	public byte[] getByteArray(String path) {
		try {
			File file = new File(path);
			FileInputStream fis = new FileInputStream(file);
			byte[] fileBytes = new byte[(int) file.length()];
			fis.read(fileBytes);
			
			fis.close();
			
			return fileBytes;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	@GET
	@Path("pictures")
	@Produces(value = MediaType.APPLICATION_JSON)
	public String getPictures() {
		
		List<Picture> pictures = DataAccessLayer.transactional(new TransactionalCode<List<Picture>>() {

			@Override
			public List<Picture> run(Session session) {
				Query query = session.createQuery("from Picture");
				List<Picture> pictures = query.list();
				
				
				
				for (Picture picture : pictures) {
					picture = (Picture) session.merge(picture);
					
//					query = session.createQuery("from Content c where c.picture_id = :id");
//					query.setParameter("id", picture.getId());
//					List<Content> contentList = query.list();
//					picture.setContentList(contentList);
				}
				
				return pictures;
			}
			
		});
		
		//postavljanje liste related pictures na null jer u ovom trenutku nije potrebno dovlaciti sve slike
//		for(Picture picture : pictures) {
//			picture.setRelatedPictures(null);
//		}
		
		String ret = new Gson().toJson(pictures);
		
//		long heapSize = Runtime.getRuntime().freeMemory();
		
		return ret;
	}
	
	@GET
	@Path("picture")
	@Produces(value = MediaType.APPLICATION_JSON)
	public String getPicture(@QueryParam("id")int id) {
		
		Picture picture = DataAccessLayer.transactional(new TransactionalCode<Picture>() {

			@Override
			public Picture run(Session session) {
				Picture picture = session.find(Picture.class, id);
				
				return picture;
			}
			
		});
		
		return new Gson().toJson(picture);
	}
	
	@POST
	@Path("delete")
	@Consumes(value = MediaType.APPLICATION_JSON)
	public String deletePicture(String jsonString) {
		JSONObject jsonObject = new JSONObject(jsonString);
		int id = jsonObject.getInt("id");
		
		DataAccessLayer.transactional(new TransactionalCode<Void>() {

			@Override
			public Void run(Session session) {
				Picture picture = session.find(Picture.class, id);
				
				//deleting every related picture from picture
				for(RelatedPicture relatedPicture : picture.getRelatedPictures()) {
					String fileName = relatedPicture.getPictureBlob();
					fileName = fileName.substring(BASE_IMAGE_URL.length(), fileName.length());
					File file = new File("images/" + fileName);
					file.delete();
					
					session.delete(relatedPicture);
				}
				
				String fileName = picture.getPictureBlob();
				fileName = fileName.substring(BASE_IMAGE_URL.length(), fileName.length());
				File file = new File("images/" + fileName);
				file.delete();
				
				session.delete(picture);
				
				return null;
			}
		});
		
		return "Success";
	}
	
	

}
